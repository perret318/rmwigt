package com.paulperret.locationreminder.tracker;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;

import com.paulperret.locationreminder.common.ConfigureLog4J;
import com.paulperret.locationreminder.common.Constants;
import com.paulperret.locationreminder.data.LocationInfo;
import com.paulperret.locationreminder.data.ReminderInfo;
import com.paulperret.locationreminder.db.DbLocationsAccessor;
import com.paulperret.locationreminder.db.DbRemindersAccessor;

public class TrackerService extends Service {
	private static final Logger LOG = Logger.getLogger(TrackerService.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	private DbRemindersAccessor reminderDB;
	private DbLocationsAccessor locationDB;

	@Override
	public void onCreate() {
		super.onCreate();

		LOG.info("Listener Service onCreate()");
		
		try {
	        Class strictModeClass=Class.forName("android.os.StrictMode");
	        Class strictModeThreadPolicyClass=Class.forName("android.os.StrictMode$ThreadPolicy");
	        Object laxPolicy = strictModeThreadPolicyClass.getField("LAX").get(null);
	        Method method_setThreadPolicy = strictModeClass.getMethod("setThreadPolicy", strictModeThreadPolicyClass );
	        method_setThreadPolicy.invoke(null,laxPolicy);
	    } catch (Exception e) {}	

		// Get a reference to the LocationManager
		setLocMgr((LocationManager)getSystemService(Context.LOCATION_SERVICE));

		// Create a new LocationListener for getting updates from the Loc. Mgr.
		setLocListener(new MyLocationListener());

		reminderDB = new DbRemindersAccessor(getApplicationContext());
		locationDB = new DbLocationsAccessor(getApplicationContext());

//		createListenerRunningNotification();
	}

	// The LocationManager
	LocationManager locMgr;

	// The LocationListener
	LocationListener locListener;

	Location currentLocation = null;

	public class MyLocationListener implements LocationListener
	{
		@Override
		public void onLocationChanged(Location loc) {

			// Check to see if the location is accurate enough
			if (isSufficientlyAccurate(loc)) {
				if (getCurrentLocation() == null) {
					LOG.info("Setting initial location: "+loc.getLatitude()+","+loc.getLongitude()+". Acc: "+loc.getAccuracy());
				}
				//LOG.info("Setting current location: "+loc.getLatitude()+","+loc.getLongitude()+". Acc: "+loc.getAccuracy());
				setCurrentLocation(loc);
				
				// TODO: Check location is within the range of any active alerts
				// TODO: Or do we want to just do it periodically?
			}
			else {
				LOG.debug("Location accuracy over threshold: "+loc.getAccuracy()+" / "+Constants.GPS_ACCURACY_THRESHOLD);
			}
		}

		@Override
		public void onProviderDisabled(String provider)
		{
			LOG.info("Provider DISABLED: "+provider);
		}

		@Override
		public void onProviderEnabled(String provider)
		{
			LOG.info("Provider ENABLED: "+provider);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras)
		{
			//			Log.i(TAG, "* STATUS CHANGED: "+provider+": "+status);
		}

	} 

	private boolean isSufficientlyAccurate(Location loc) {
		float accuracy = loc.getAccuracy();

		if (accuracy <= Constants.GPS_ACCURACY_THRESHOLD) {
			return true;
		}
		else {
			return false;
		}
	}

	// TODO: Save some time by caching which Locations in the DB have already been checked
	private boolean isWithinRange(Long reminderId, Location location) {
		boolean returnVal = false;
		
		ReminderInfo reminderInfo = reminderDB.getReminderById(reminderId);
		LOG.info("Read alarm "+reminderId+": "+reminderInfo.getNote());
		
		String locationName = reminderInfo.getLocationName();
		LocationInfo locationInfo = locationDB.getLocationByName(locationName);
		
		return returnVal;
	}

	public LocationManager getLocMgr() {
		return locMgr;
	}

	public void setLocMgr(LocationManager locMgr) {
		this.locMgr = locMgr;
	}

	public LocationListener getLocListener() {
		return locListener;
	}

	public void setLocListener(LocationListener locListener) {
		this.locListener = locListener;
	}

	public Location getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(Location currentLocation) {
		this.currentLocation = currentLocation;
	}


	/*
	 * Service Binder stuff.  Make sure we really want to do it this way...
	 */
	private final IBinder binder = new ListenerBinder();

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return Service.START_STICKY;
	}

	public class ListenerBinder extends Binder {

		public TrackerService getService() {
			// Return this instance of the service so clients can call public methods
			return TrackerService.this;
		}
	}
}
