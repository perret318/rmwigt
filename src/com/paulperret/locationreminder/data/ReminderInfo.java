package com.paulperret.locationreminder.data;


public class ReminderInfo {
//	private String name;
	private String note;
	private String locationName;
	private int range;
	private boolean playRingtone;
	private String ringtone;
	private boolean vibrate;
	private boolean active;
	private long id;
	

	public ReminderInfo(String note, String locationName,
			int range, boolean playRingtone, String ringtone, boolean vibrate, boolean active, long id) {
		super();
//		this.name = name;
		this.note = note;
		this.locationName = locationName;
		this.range = range;
		this.playRingtone = playRingtone;
		this.ringtone = ringtone;
		this.vibrate = vibrate;
		this.active = active;
		this.id = id;
	}
	
//	public ReminderInfo(JSONObject jsonObj) throws JSONException {
//		super();
////		this.name = jsonObj.getString(Constants.KEY_REMINDER_NAME);
//		this.note = jsonObj.getString(Constants.KEY_REMINDER_NOTE);
//		this.locationName = jsonObj.getString(Constants.KEY_REMINDER_LOCATIONNAME);
//		this.range = jsonObj.getInt(Constants.KEY_REMINDER_RANGE);
//		this.active = jsonObj.getBoolean(Constants.KEY_REMINDER_ACTIVE);
//	}
//	
//	public JSONObject toJSON() throws JSONException {
//		JSONObject jsonObj = new JSONObject();
////		jsonObj.put(Constants.KEY_REMINDER_NAME, this.name);
//		jsonObj.put(Constants.KEY_REMINDER_NOTE, this.note);
//		jsonObj.put(Constants.KEY_REMINDER_LOCATIONNAME, this.locationName);
//		jsonObj.put(Constants.KEY_REMINDER_RANGE, this.range);
//		jsonObj.put(Constants.KEY_REMINDER_ACTIVE, this.active);
//		
//		return jsonObj;
//	}
	
//	public String getName() {
//		return name;
//	}
//	public void setName(String name) {
//		this.name = name;
//	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getRingtone() {
		return ringtone;
	}
	public void setRingtone(String ringtone) {
		this.ringtone = ringtone;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		this.range = range;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isVibrate() {
		return vibrate;
	}

	public void setVibrate(boolean vibrate) {
		this.vibrate = vibrate;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public boolean isPlayRingtone() {
		return playRingtone;
	}

	public void setPlayRingtone(boolean playRingtone) {
		this.playRingtone = playRingtone;
	}

}
