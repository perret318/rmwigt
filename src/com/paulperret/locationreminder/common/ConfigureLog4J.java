package com.paulperret.locationreminder.common;

import org.apache.log4j.Level;

import android.os.Environment;
import android.util.Log;
import de.mindpipe.android.logging.log4j.LogConfigurator;
/**
 * Simply place a class like this in your Android applications classpath.
 */
public class ConfigureLog4J {
	
    private static String TAG = "ConfigureLog4J";

    static {
        final LogConfigurator logConfigurator = new LogConfigurator();

        /*
		File logFile = new File(Constants.LOG_FILE_PATH);

		if (!(logFile.exists())) {
			System.out.println("Creating log file.");
			
			// Create directory structure
			new File(Constants.LOG_PATH).mkdirs();
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		*/
         
        String status = Environment.getExternalStorageState();
        Log.i(TAG,"External Storage Status: "+status);
        
        if (status.equals(Environment.MEDIA_MOUNTED)) {
        	Log.i(TAG,"SD Card OK, setting file logging params");
        	logConfigurator.setFileName(Constants.LOG_FILE_PATH);
            logConfigurator.setFilePattern("%d{yyyy-MM-dd HH:mm:ss} [%p] %t %c{1}: %L: (%M) %m%n");
            logConfigurator.setUseFileAppender(true);
        }
        else {
        	Log.i(TAG,"*** SD Card not present. Not using file logging.");
        	logConfigurator.setUseFileAppender(false);
        }
        

        logConfigurator.setRootLevel(Level.DEBUG);
        logConfigurator.setLogCatPattern("%d{yyyy-MM-dd HH:mm:ss} [%p] %t %c{1}: %L: (%M) %m%n");
        
        // Set log level of a specific logger
        logConfigurator.setLevel("com.geocent.game", Level.INFO);
        logConfigurator.configure();
    }
}