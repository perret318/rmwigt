package com.paulperret.locationreminder.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class AddLocationDialogFragment  extends DialogFragment implements DialogInterface.OnClickListener {

	private final int CHOOSE_FAVORITE = 0;
	private final int PICK_FROM_MAP = 1;
	
	private String[] choicesArray = {
			"Choose from my Favorite Locations", "Pick the location on a map" };

	public AddLocationDialogFragment () {
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("How would you like to choose a location?");
		builder.setNegativeButton("Cancel", this);
		builder.setSingleChoiceItems(choicesArray, -1, new SingleChoiceListener());
		return builder.create();

	}

	@Override
	public void onClick(DialogInterface dialogInterface, int i) {
	}

	private class SingleChoiceListener implements DialogInterface.OnClickListener {
		@Override
		public void onClick(DialogInterface dialog, int item) {
			AddLocationDialogListener targetFragment = (AddLocationDialogListener) getActivity();
            
			if (item == CHOOSE_FAVORITE) {
	            targetFragment.onChooseLocationFromFavorites();
			}
			else if (item == PICK_FROM_MAP) {
	            targetFragment.onPickLocationFromMap();
			}
			
			dismiss();
		}
	}
	
	public interface AddLocationDialogListener {
        void onChooseLocationFromFavorites();
        void onPickLocationFromMap();
    }

}
