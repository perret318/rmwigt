package com.paulperret.locationreminder.common;

import org.apache.log4j.Logger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.paulperret.locationreminder.R;

public class Utils {

	public final static Logger LOG = Logger.getLogger(Utils.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	public static void addReminderRow(final Context context, final LinearLayout reminderListLayout,
			final String name, final String location,  boolean isActive,
			final OnClickListener onClickListener) {

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View reminderRow = inflater.inflate(R.layout.reminderlistitem, null);

		reminderListLayout.addView(reminderRow);

		final TextView reminderNameView = (TextView) reminderRow.findViewById(R.id.reminderName);
		reminderNameView.setText(name);

		final TextView reminderLocationView = (TextView) reminderRow.findViewById(R.id.reminderLocation);
		reminderLocationView.setText(location);

		reminderRow.setOnClickListener(onClickListener);
	}

	public static void addLocationRow(final Context context, final LinearLayout locationListLayout,
			final String name, final String address, final OnClickListener onClickListener) {

		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View locationRow = inflater.inflate(R.layout.locationlistitem, null);

		locationListLayout.addView(locationRow);

		final TextView locationNameView = (TextView) locationRow.findViewById(R.id.locationName);
		locationNameView.setText(name);

		final TextView locationAddressView = (TextView) locationRow.findViewById(R.id.locationAddress);
		locationAddressView.setText(address);

		locationRow.setOnClickListener(onClickListener);
	}

}
