package com.paulperret.locationreminder.common;

import android.os.Environment;

public class Constants {
	public static final String APP_DATA_PATH = Environment.getExternalStorageDirectory() + "/"+ "RMWIGT" + "/";
	public static final String LOG_PATH = APP_DATA_PATH + "logs/";
	public static final String LOG_FILE_PATH = LOG_PATH + "log.txt";
	public static final String REMINDER_FILE_PATH = APP_DATA_PATH + "reminderData.json";

	public static final float GPS_ACCURACY_THRESHOLD = 128.0f;
	
	public static final String KEY_LOCATION_NAME = "locationName";
	public static final String KEY_LOCATION_ID = "id";
	
	public static String KEY_REMINDER_NAME = "reminderName";
	public static String KEY_REMINDER_NOTE = "note";
	public static String KEY_REMINDER_LOCATIONNAME = "locationName";
	public static String KEY_REMINDER_RANGE = "range";
	public static String KEY_REMINDER_ACTIVE = "active";
	public static String KEY_REMINDER_ID = "id";
	public static String KEY_REMINDER_VIBRATE = "vibrate";
	public static String KEY_REMINDER_PLAY_RINGTONE = "playRingtone";
	public static String KEY_REMINDER_RINGTONE = "ringtone";
	
	public static final String KEY_MAP_CHOSEN_POINT_LAT = "pointChosenLat";
	public static final String KEY_MAP_CHOSEN_POINT_LON = "pointChosenLon";

	public static String ANNOUNCE_ID_LOCATION_UPDATE = "LocationUpdate";

	public static final String KEY_MSG_TYPE = "msgType";
	public static final String KEY_LAT = "lat";
	public static final String KEY_LON = "lon";
	public static final String KEY_ACCURACY = "accuracy";
	
	public static final int DB_VERSION = 1;
	public static final String DB_NAME_LOCATION_REMINDER = "locreminder";

	public static final String PROMPT_VIEW_EDIT_LOCATION = "View/Edit this location on the map";
	public static final String PROMPT_DELETE_LOCATION = "Delete this location";
	
	// Dialog ID's
	public static final int DIALOG_GETTING_LOCATION = 1;
	public static final int DIALOG_CREATE_LOCATION_CHOICE = 2;
	public static final int DIALOG_NAME_LOCATION = 3;
	
	// Activity return codes
	public static final int CODE_MAP_POINT_REQUESTED = 1;
	public static final int CODE_MAP_POINT_RETURNED = 2;
	public static final int CODE_NEW_LOCATION_NAME_REQUEST = 3;
	public static final int CODE_MAP_EDIT_REQUEST = 4;
}
