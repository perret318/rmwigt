package com.paulperret.locationreminder.common;

import org.apache.log4j.Logger;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.paulperret.locationreminder.R;

public class NamePromptDialogFragment extends DialogFragment implements OnEditorActionListener {

	private EditText mEditText;

	private static final Logger LOG = Logger.getLogger(NamePromptDialogFragment.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	public interface NamePromptDialogListener {
        void onFinishEditDialog(String inputText);
    }
	
	public NamePromptDialogFragment() {
		// Empty constructor required for DialogFragment
	}

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.namepromptdialoglayout, container);
        mEditText = (EditText) view.findViewById(R.id.nameEditText);
        getDialog().setTitle("Give this location a name");

        // Show soft keyboard automatically
        mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        mEditText.setOnEditorActionListener(this);

        return view;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (EditorInfo.IME_ACTION_DONE == actionId) {
        	LOG.info("onEditorAction called: "+mEditText.getText().toString());
            // Return input text to activity
            NamePromptDialogListener targetFragment = (NamePromptDialogListener) getTargetFragment();
            targetFragment.onFinishEditDialog(mEditText.getText().toString());
            this.dismiss();
            return true;
        }
        else {
        	LOG.info("onEditorAction called but not for DONE action");
        }
        return false;
    }
}
