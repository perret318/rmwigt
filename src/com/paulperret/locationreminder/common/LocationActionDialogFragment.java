package com.paulperret.locationreminder.common;

import org.apache.log4j.Logger;

import com.paulperret.locationreminder.ui.ViewReminderActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

public class LocationActionDialogFragment extends DialogFragment {

//	String locationName;
	private static final Logger LOG = Logger.getLogger(LocationActionDialogFragment.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	public LocationActionDialogFragment() {
//		locationName = getArguments().getString(Constants.KEY_LOCATION_NAME);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		LOG.info("Creating Location Action dialog");
		
		final String locationName = getArguments().getString(Constants.KEY_LOCATION_NAME);
		final Long locationId = getArguments().getLong(Constants.KEY_LOCATION_ID);
		final Double lat = getArguments().getDouble(Constants.KEY_LAT);
		final Double lon = getArguments().getDouble(Constants.KEY_LON);
		
		final String[] items = {
				Constants.PROMPT_VIEW_EDIT_LOCATION,
				Constants.PROMPT_DELETE_LOCATION
		};

		final ListAdapter adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.select_dialog_item,
				android.R.id.text1,
				items);

		return new AlertDialog.Builder(getActivity())
		.setCancelable(true)
		.setTitle(locationName)
		.setAdapter(adapter, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				LocationActionDialogListener targetFragment;
				try {
					targetFragment = (LocationActionDialogListener) getActivity();
				}
				catch (ClassCastException e) {
					targetFragment = (LocationActionDialogListener) getTargetFragment();
				}
				
				if (which == 0) {
					LOG.info("Show map option clicked");
					targetFragment.onShowOnMap(locationId, locationName, lat, lon);
				}
				if (which == 1) {
					LOG.info("Delete location option clicked");
					targetFragment.onDeleteLocation(locationId);
				}
			}
		})
		.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		})
		.create();
	}

	public interface LocationActionDialogListener {
		void onDeleteLocation(Long locationId);
		void onShowOnMap(Long locationId, String locationName, Double lat, Double lon);
	}
}
