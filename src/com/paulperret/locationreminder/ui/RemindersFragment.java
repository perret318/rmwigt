package com.paulperret.locationreminder.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragment;
import com.paulperret.locationreminder.R;
import com.paulperret.locationreminder.common.ConfigureLog4J;
import com.paulperret.locationreminder.common.Constants;
import com.paulperret.locationreminder.common.Utils;
import com.paulperret.locationreminder.data.ReminderInfo;
import com.paulperret.locationreminder.db.DbRemindersAccessor;

public class RemindersFragment extends SherlockFragment implements ActionBar.TabListener {

	private static final Logger LOG = Logger.getLogger(RemindersFragment.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	private DbRemindersAccessor db;
	private Fragment mFragment;

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mFragment = new RemindersFragment();
		// Attach fragment1.xml layout
		ft.add(android.R.id.content, mFragment);
		ft.attach(mFragment);
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// Remove fragment1.xml layout
		ft.remove(mFragment);
	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().setContentView(R.layout.activity_main);

		configureButtons();

		// service = new AlarmClockServiceBinder(getApplicationContext());
		db = new DbRemindersAccessor(getActivity().getApplicationContext());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		db.closeConnections();
	}

	@Override
	public void onStart() {
		super.onStart();

		refreshRemindersList();
	}

	private void configureButtons() {
		RelativeLayout addReminderLayout = (RelativeLayout)getActivity().findViewById(R.id.addReminderLayout);
		addReminderLayout.setOnClickListener(addReminderClickListener);
	}

	private OnClickListener addReminderClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			LOG.info("Add new reminder clicked");
			Intent intent = new Intent(getActivity().getApplicationContext(), ViewReminderActivity.class);
			startActivity(intent);

			// TEST STUFF
			//			db.createReminder("Test "+new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()), "Home", 50, false, "soundUri", true, true);
			//			refreshRemindersList();
		}
	};

	private synchronized void refreshRemindersList() {
		LOG.info("Refreshing Reminders List");

		LinearLayout reminderListLayout = (LinearLayout) getActivity().findViewById(R.id.reminderListLayout);
		reminderListLayout.removeAllViews();

		// Read the reminder list from the DB
		List<Long> reminderIds = db.getAllReminderIDs();
		List<ReminderInfo> reminderList = new ArrayList<ReminderInfo>();
		for (Long id : reminderIds) {
			ReminderInfo reminderInfo = db.getReminderById(id);
			LOG.info("Read alarm "+id+": "+reminderInfo.getNote());
			reminderList.add(reminderInfo);
		}

		for (ReminderInfo reminder : reminderList) {
			final boolean isActive = reminder.isActive();

			final String note = reminder.getNote();
			final Long id = reminder.getId();
			final String locationName = reminder.getLocationName();
			final int range = reminder.getRange();
			final boolean active = reminder.isActive();
			final boolean vibrate = reminder.isVibrate();
			final boolean playRingtone = reminder.isPlayRingtone();
			final String ringtone = reminder.getRingtone();

			Utils.addReminderRow(getActivity().getApplicationContext(), reminderListLayout, note, locationName, isActive,
					new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getActivity().getApplicationContext(), ViewReminderActivity.class);
					intent.putExtra(Constants.KEY_REMINDER_ID, id);
					intent.putExtra(Constants.KEY_REMINDER_NOTE, note);
					intent.putExtra(Constants.KEY_REMINDER_LOCATIONNAME, locationName);
					intent.putExtra(Constants.KEY_REMINDER_RANGE, range);
					intent.putExtra(Constants.KEY_REMINDER_ACTIVE, active);
					intent.putExtra(Constants.KEY_REMINDER_VIBRATE, vibrate);
					intent.putExtra(Constants.KEY_REMINDER_PLAY_RINGTONE, playRingtone);
					intent.putExtra(Constants.KEY_REMINDER_RINGTONE, ringtone);

					startActivity(intent);
				}
			});
		}	
	}
}


