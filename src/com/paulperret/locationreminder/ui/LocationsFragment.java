package com.paulperret.locationreminder.ui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragment;
import com.paulperret.locationreminder.R;
import com.paulperret.locationreminder.common.ConfigureLog4J;
import com.paulperret.locationreminder.common.Constants;
import com.paulperret.locationreminder.common.LocationActionDialogFragment;
import com.paulperret.locationreminder.common.LocationActionDialogFragment.LocationActionDialogListener;
import com.paulperret.locationreminder.common.NamePromptDialogFragment;
import com.paulperret.locationreminder.common.NamePromptDialogFragment.NamePromptDialogListener;
import com.paulperret.locationreminder.common.Utils;
import com.paulperret.locationreminder.data.LocationInfo;
import com.paulperret.locationreminder.db.DbLocationsAccessor;

public class LocationsFragment extends SherlockFragment implements ActionBar.TabListener, NamePromptDialogListener, LocationActionDialogListener {

	private static final Logger LOG = Logger.getLogger(LocationsFragment.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	private DbLocationsAccessor db;
	private Fragment mFragment;

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		mFragment = new LocationsFragment();
		// Attach fragment1.xml layout
		ft.add(android.R.id.content, mFragment);
		ft.attach(mFragment);
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// Remove fragment1.xml layout
		ft.remove(mFragment);
	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().setContentView(R.layout.viewlocationslayout);

		configureButtons();

		db = new DbLocationsAccessor(getActivity().getApplicationContext());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		db.closeConnections();
	}

	@Override
	public void onStart() {
		super.onStart();

		refreshLocationsList();
	}

	private void configureButtons() {
		RelativeLayout addLocationLayout = (RelativeLayout)getActivity().findViewById(R.id.addLocationLayout);
		addLocationLayout.setOnClickListener(addLocationClickListener);
	}

	private OnClickListener addLocationClickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			LOG.info("Add new Location clicked");
			Intent intent = new Intent(getActivity().getApplicationContext(), ChooseLocationMapActivity.class);

			int requestCode = Constants.CODE_MAP_POINT_REQUESTED;
			startActivityForResult(intent, requestCode);
		}
	};

	private synchronized void refreshLocationsList() {
		LOG.info("Refreshing Locations List");

		LinearLayout locationListLayout = (LinearLayout) getActivity().findViewById(R.id.locationListLayout);
		locationListLayout.removeAllViews();

		// Read the location list from the DB
		// TODO: Only get the favorite Locations
		List<Long> locationIds = db.getAllLocationIDs();
		List<LocationInfo> locationList = new ArrayList<LocationInfo>();
		for (Long id : locationIds) {
			LocationInfo locationInfo = db.getLocationById(id);
			locationList.add(locationInfo);
		}

		for (final LocationInfo location : locationList) {
			final Long id = location.getId();
			final String name = location.getName();
			final String address = location.getAddress();
			final Double lat = location.getLatitude();
			final Double lon = location.getLongitude();

			Utils.addLocationRow(getActivity().getApplicationContext(), locationListLayout, name, address,
					new OnClickListener() {

				@Override
				public void onClick(View v) {
					showLocationActionDialog(id, name, lat, lon);
				}
			});
		}	
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		LOG.info("OnActivityResult returned. RequestCode: "+requestCode+", ResultCode: "+resultCode);

		if (resultCode == Constants.CODE_MAP_POINT_RETURNED) {
			if (data != null) {
				newLocationLatE6 = data.getIntExtra(Constants.KEY_MAP_CHOSEN_POINT_LAT, -1);
				newLocationLonE6 = data.getIntExtra(Constants.KEY_MAP_CHOSEN_POINT_LON, -1);
				
				LOG.info("Point returned from map: "+newLocationLatE6+","+newLocationLonE6);

				// TODO: Reverse GeoCode the lat/lon to an address
				newLocationAddress = getAddressFromCoords();

				if (requestCode == Constants.CODE_MAP_POINT_REQUESTED) {
					// Prompt for Name - the handler will take care of creating the new Location Favorite
					showEditDialog();
				}
				else if (requestCode == Constants.CODE_MAP_EDIT_REQUEST) {
					// Edit the existing location
					Long locationId = data.getLongExtra(Constants.KEY_LOCATION_ID, -1);
					LOG.info("Updating Location: "+locationId);
					LocationInfo existingLocation = db.getLocationById(locationId);
					db.updateLocation(locationId, existingLocation.getName(), newLocationAddress,
							newLocationLatE6 / 1E6, newLocationLonE6 / 1E6, true);
					
					refreshLocationsList();
				}
			}
			else {
				LOG.info("Map activity returned null intent.");
			}
		}
	}

	private String getAddressFromCoords() {
		JSONObject jsonObject = getLocationInfo();

		try {			
			List<String> addressList = parseAddresses(jsonObject);

			if ((addressList != null) && (addressList.size() > 0)) {
				return addressList.get(0);
			}
			else {
				return "Unknown location";
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}

	}

	private JSONObject getLocationInfo() {

		HttpGet httpGet = new HttpGet("http://maps.google.com/maps/api/geocode/json?latlng="+
				(newLocationLatE6 / 1E6) +","+ (newLocationLonE6 / 1E6) +"&sensor=true");
		HttpClient client = new DefaultHttpClient();
		HttpResponse response;
		StringBuilder stringBuilder = new StringBuilder();

		try {
			response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream stream = entity.getContent();
			int b;
			while ((b = stream.read()) != -1) {
				stringBuilder.append((char) b);
			}
		}
		catch (ClientProtocolException e) {} 
		catch (IOException e) {}

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject(stringBuilder.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return jsonObject;
	}

	private List<String> parseAddresses(JSONObject jsonObj) throws JSONException {
		List<String> addressList = new ArrayList<String>();

		JSONArray locationArray = jsonObj.getJSONArray("results");

		for (int i=0; i<locationArray.length(); i++) {
			addressList.add(locationArray.getJSONObject(i).getString("formatted_address"));
		}

		return addressList;
	}

	private String newLocationName;
	private String newLocationAddress;
	private Integer newLocationLatE6;
	private Integer newLocationLonE6;

	private void showEditDialog() {
		FragmentManager fm = getActivity().getSupportFragmentManager();
		NamePromptDialogFragment namePromptDialog = new NamePromptDialogFragment();
		namePromptDialog.setTargetFragment(this, Constants.CODE_NEW_LOCATION_NAME_REQUEST);
		namePromptDialog.show(fm, "fragment_edit_name");
	}

	@Override
	public void onFinishEditDialog(String inputText) {
		LOG.info("onFinishEditDialog called for: "+inputText);
		newLocationName = inputText;

		if (newLocationName == null) {
			LOG.info("New location name is null. Exiting.");
			return;
		}

		// Store new location in DB
		LocationInfo newLocInfo = db.createLocation(newLocationName, newLocationAddress, newLocationLatE6 / 1E6, newLocationLonE6 / 1E6, true);

		LOG.info("Created new Location: "+newLocInfo.getName());

		newLocationName = null;
		newLocationLatE6 = null;
		newLocationLonE6 = null;
		newLocationAddress = null;

		// Refresh the list
		refreshLocationsList();
	}


	private void showLocationActionDialog(Long id, String locationName, Double lat, Double lon) {
		FragmentManager fm = getActivity().getSupportFragmentManager();
		LocationActionDialogFragment locationActionDialog = new LocationActionDialogFragment();
		locationActionDialog.setTargetFragment(this, 0);
		Bundle args = new Bundle();
		args.putString(Constants.KEY_LOCATION_NAME, locationName);
		args.putDouble(Constants.KEY_LAT, lat);
		args.putDouble(Constants.KEY_LON, lon);
		args.putLong(Constants.KEY_LOCATION_ID, id);
		locationActionDialog.setArguments(args);
		locationActionDialog.show(fm, "fragment_location_action");
	}

	@Override
	public void onDeleteLocation(Long locationId) {
		LOG.info("Deleting location: "+locationId);
		db.deleteLocation(locationId);
		refreshLocationsList();
	}

	@Override
	public void onShowOnMap(Long locationId, String locationName, Double lat, Double lon) {
		LOG.info("Showing location on map: "+locationName);
		
		Intent intent = new Intent(getActivity().getApplicationContext(), ChooseLocationMapActivity.class);
		int requestCode = Constants.CODE_MAP_EDIT_REQUEST;
		
		Bundle bundle = new Bundle();
		bundle.putDouble(Constants.KEY_LAT, lat);
		bundle.putDouble(Constants.KEY_LON, lon);
		bundle.putString(Constants.KEY_LOCATION_NAME, locationName);
		bundle.putLong(Constants.KEY_LOCATION_ID, locationId);
		intent.putExtras(bundle);
		
		startActivityForResult(intent, requestCode);
	}

	//	private int choice = 0;
	//	@Override
	//	protected Dialog onCreateDialog(int id, Bundle bundle) {
	//		Dialog dialog = null;
	//
	//		switch (id) {
	//		case Constants.DIALOG_CREATE_LOCATION_CHOICE:
	//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
	//			LayoutInflater inflater = getLayoutInflater();
	//
	//			// Inflate and set the layout for the dialog
	//			// Pass null as the parent view because its going in the dialog layout
	//			builder.setView(inflater.inflate(R.layout.chooselocationdialoglayout, null))
	//
	//			.setNegativeButton("Cancel", null);
	//
	//
	//			dialog = builder.create();
	//			break;
	//
	//		}
	//
	//		return dialog;
	//	}
}
