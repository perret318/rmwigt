package com.paulperret.locationreminder.ui;

import android.os.Bundle;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.paulperret.locationreminder.R;


public class MainActivity extends SherlockFragmentActivity {

	// Declare Tab Variable
    Tab tab;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Create the Actionbar
        ActionBar actionBar = getSupportActionBar();
 
        // Hide Actionbar Icon
        actionBar.setDisplayShowHomeEnabled(false);
 
        // Hide Actionbar Title
        actionBar.setDisplayShowTitleEnabled(false);
 
        // Create Actionbar Tabs
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
 
        // Create first Tab
        tab = actionBar.newTab().setTabListener(new RemindersFragment());
        tab.setText("Reminders");
        tab.setIcon(R.drawable.reminder);
        actionBar.addTab(tab);
 
        // Create Second Tab
        tab = actionBar.newTab().setTabListener(new LocationsFragment());
        tab.setText("Locations");
        tab.setIcon(R.drawable.map1);
        actionBar.addTab(tab);
 
//        // Create Third Tab
//        tab = actionBar.newTab().setTabListener(new FragmentsTab3());
//        // Set Tab Title
//        tab.setText("Tab3");
//        actionBar.addTab(tab);
    }
}