package com.paulperret.locationreminder.ui;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.paulperret.locationreminder.R;
import com.paulperret.locationreminder.common.ConfigureLog4J;
import com.paulperret.locationreminder.common.Constants;

public class ChooseLocationMapActivity extends MapActivity 
{    
	private MapView mapView; 
	private MapController mc;
	private GeoPoint pointToReturn;

	// The LocationManager
	LocationManager locMgr;

	// The LocationListener
	LocationListener locListener;

	private final Logger LOG = Logger.getLogger(ChooseLocationMapActivity.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chooselocationmaplayout);

		configureButtons();

		mapView = (MapView) findViewById(R.id.mapView);
		mc = mapView.getController();
		mapView.setBuiltInZoomControls(true);

		//---Add a location marker---
		MapOverlay mapOverlay = new MapOverlay();
		List<Overlay> listOfOverlays = mapView.getOverlays();
		listOfOverlays.clear();
		listOfOverlays.add(mapOverlay);   

		// If a point is passed in already, set it
		if ((getIntent().getExtras() != null) &&
				(getIntent().getExtras().containsKey(Constants.KEY_LAT))) {
			LOG.info("Point passed in, marking that on the map");
			Double lat = getIntent().getExtras().getDouble(Constants.KEY_LAT);
			Double lon = getIntent().getExtras().getDouble(Constants.KEY_LON);
			pointToReturn = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
			mc.animateTo(pointToReturn);
			mc.setZoom(14); 
		}

		mapView.invalidate();
	}

	@Override
	protected void onStart() {
		super.onStart();

		// If we're displaying an existing location, don't try to center where we are.
		// We're already centering on the point passed in.
		if (((getIntent().getExtras() != null) &&
				!(getIntent().getExtras().containsKey(Constants.KEY_LAT)))) {
			// Get a reference to the LocationManager
			setLocMgr((LocationManager) getSystemService(Context.LOCATION_SERVICE));

			// Create a new LocationListener for getting updates from the Loc. Mgr.
			setLocListener(new MyLocationListener());

			// Display waiting for position dialog
			showDialog(Constants.DIALOG_GETTING_LOCATION);

			// TODO: Try last known location
			startGpsUpdates();
		}
	}

	@Override
	protected void onStop() {
		super.onStop();

		if (getLocMgr() != null) {
			getLocMgr().removeUpdates(getLocListener());
		}
	}

	private void configureButtons() {
		Button selectLocationButton = (Button)findViewById(R.id.selectLocationButton);
		selectLocationButton.setOnClickListener(selectLocationButtonClickListener);

		Button searchButton = (Button)findViewById(R.id.searchButton);
		searchButton.setOnClickListener(searchButtonClickListener);
	}

	private void startGpsUpdates() {

		LOG.info("Starting GPS updates");

		// Request updates from the Location Manager
		getLocMgr().requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, getLocListener());

		getLocMgr().requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, getLocListener());
	}

	private OnClickListener selectLocationButtonClickListener = new OnClickListener() {

		public void onClick(View v) {
			LOG.info("Select Location Button clicked");	
			Intent intent=new Intent();

			if (pointToReturn != null) {
				intent.putExtra(Constants.KEY_MAP_CHOSEN_POINT_LAT, pointToReturn.getLatitudeE6());
				intent.putExtra(Constants.KEY_MAP_CHOSEN_POINT_LON, pointToReturn.getLongitudeE6());
				if (getIntent().getExtras() != null) {
					intent.putExtra(Constants.KEY_LOCATION_NAME, getIntent().getExtras().getString(Constants.KEY_LOCATION_NAME));
					intent.putExtra(Constants.KEY_LOCATION_ID, getIntent().getExtras().getLong(Constants.KEY_LOCATION_ID));
				}
			}

			setResult(Constants.CODE_MAP_POINT_RETURNED, intent);
			finish();
		}
	};

	private OnClickListener searchButtonClickListener = new OnClickListener() {
		public void onClick(View v) {

			EditText searchEditText = (EditText) findViewById(R.id.searchEditText);
			String searchText = searchEditText.getText().toString();
			LOG.info("Search button clicked for text: "+searchText);

			List<Address> foundGeocode = null;
			try {
				foundGeocode = new Geocoder(ChooseLocationMapActivity.this).getFromLocationName(searchText, 1);
				double lat = foundGeocode.get(0).getLatitude();
				double lon = foundGeocode.get(0).getLongitude();

				LOG.info("GeoCoder found the following location: "+lat+", "+lon);

				// Center the map to the location
				GeoPoint currentLocationPoint = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));
				mc.animateTo(currentLocationPoint);

				// Set the pointToReturn, and invalidate the map, which will cause the overlay to draw the pin
				pointToReturn = currentLocationPoint;
				mapView = (MapView) findViewById(R.id.mapView);
				mapView.invalidate();

				// Make the keyboard go away
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);

			}
			catch (IOException e) {
				LOG.error("Execption trying to Geocode: "+searchText,e);
			}

		}
	};

	class MapOverlay extends com.google.android.maps.Overlay
	{
		private boolean isPinch = false;

		@Override
		public boolean draw(Canvas canvas, MapView mapView, 
				boolean shadow, long when) 
		{
			super.draw(canvas, mapView, shadow);                   

			//---translate the GeoPoint to screen pixels---
			Point screenPts = new Point();
			if (pointToReturn != null) {
				mapView.getProjection().toPixels(pointToReturn, screenPts);

				//---add the marker---
				Bitmap bmp = BitmapFactory.decodeResource(
						getResources(), R.drawable.map_pushpin);            
				canvas.drawBitmap(bmp, screenPts.x, screenPts.y-50, null);    
			}
			return true;
		}

		@Override
		public boolean onTap(GeoPoint p, MapView mapView) {

			if ( isPinch ) {
				return false;
			}
			else {			
				pointToReturn = p;
				mapView = (MapView) findViewById(R.id.mapView);
				mapView.invalidate();

				return false;
			}
		}

		@Override
		public boolean onTouchEvent(MotionEvent e, MapView mapView)
		{
			int fingers = e.getPointerCount();
			if( e.getAction()==MotionEvent.ACTION_DOWN ){
				isPinch=false;  // Touch DOWN, don't know if it's a pinch yet
			}
			if( e.getAction()==MotionEvent.ACTION_MOVE && fingers==2 ){
				isPinch=true;   // Two fingers, def a pinch
			}
			return super.onTouchEvent(e,mapView);
		}
	} 


	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}


	public class MyLocationListener implements LocationListener {
		/*
		 * When a location update comes in, set that to the current location,
		 * add it to the route list, and send out the announcement of the
		 * update.
		 * 
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.location.LocationListener#onLocationChanged(android.location
		 * .Location)
		 */
		@Override
		public void onLocationChanged(Location loc) {

			if (loc == null) {
				LOG.info("Location is null.");
				return;
			}

			LOG.info("New Loc: Lat:" + loc.getLatitude() + " Long:"
					+ loc.getLongitude() + " Acc:" + loc.getAccuracy() + " Provider: "
					+ loc.getProvider()+" Speed: "+loc.getSpeed()+" m/s");

			if (isSufficientlyAccurate(loc)) {
				LOG.info("Accuracy reached." + " Threshold: " + getAccuracyThreshold());

				getLocMgr().removeUpdates(getLocListener());
				dismissDialog(Constants.DIALOG_GETTING_LOCATION);

				double lat = loc.getLatitude();
				double lng = loc.getLongitude();

				GeoPoint currentLocationPoint = new GeoPoint(
						(int) (lat * 1E6), (int) (lng * 1E6));

				mc.animateTo(currentLocationPoint);
				mc.setZoom(14); 
			} else {
				LOG.info("Accuracy not suffient: " + loc.getAccuracy() + "Threshold: " + getAccuracyThreshold());
			}
		}

		@Override
		public void onProviderDisabled(String provider) {
			LOG.info(provider + ": * PROVIDER DISABLED *");

			// Stop updates from the Location Manager
			// getLocMgr().removeUpdates(getLocListener());
		}

		@Override
		public void onProviderEnabled(String provider) {
			LOG.info(provider + ": * PROVIDER ENABLED *");
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// LOG.info( "* STATUS CHANGED: "+provider+": "+status);
		}
	}


	@Override
	protected Dialog onCreateDialog(int id, final Bundle args) {
		Dialog dialog = null;

		switch (id) {
		case Constants.DIALOG_GETTING_LOCATION:
			ProgressDialog fetchingDialog = new ProgressDialog(this);
			fetchingDialog.setTitle("Please wait...");
			fetchingDialog.setMessage("Obtaining current location");
			fetchingDialog.setCanceledOnTouchOutside(false);
			fetchingDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					getLocMgr().removeUpdates(getLocListener());
					removeDialog(Constants.DIALOG_GETTING_LOCATION);
				}
			});
			dialog = fetchingDialog;
			break;

		default:
			LOG.info("Unknown dialog ID:" + id);
		}

		return dialog;
	}

	public float getAccuracyThreshold() {
		return 2000;
	}

	private LocationManager getLocMgr() {
		return locMgr;
	}

	private void setLocMgr(LocationManager locMgr) {
		this.locMgr = locMgr;
	}

	private LocationListener getLocListener() {
		return locListener;
	}

	private void setLocListener(LocationListener locListener) {
		this.locListener = locListener;
	}

	private boolean isSufficientlyAccurate(Location loc) {

		if (loc == null) {
			return false;
		}

		float accuracy = loc.getAccuracy();

		if (accuracy <= getAccuracyThreshold()) {
			return true;
		}

		return false;
	}

}
