package com.paulperret.locationreminder.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.preference.RingtonePreference;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.paulperret.locationreminder.R;
import com.paulperret.locationreminder.common.ConfigureLog4J;
import com.paulperret.locationreminder.common.Constants;
import com.paulperret.locationreminder.common.NamePromptDialogFragment;
import com.paulperret.locationreminder.data.LocationInfo;
import com.paulperret.locationreminder.db.DbLocationsAccessor;
import com.paulperret.locationreminder.db.DbRemindersAccessor;

public class ViewReminderActivity extends PreferenceActivity {

	private static final Logger LOG = Logger.getLogger(ViewReminderActivity.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	private String note = "";
	private String location = "";
	private int range = 100;
	private boolean active = true;
	private boolean vibrate = true;
	private Long id = -1l;
	private boolean playRingtone = false;
	private String ringtone = ""; // TODO: Initialize with default ringtone

	private DbLocationsAccessor db;

	private final String ADD_NEW_LOCATION = "Add new location";

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settingsviewreminderlayout);
		setContentView(R.layout.viewreminderlayout);

		db = new DbLocationsAccessor(getApplicationContext());

		initializeViews();

		configureButtons();

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		db.closeConnections();
	}


	@SuppressWarnings("deprecation")
	private void initializeViews() {


		//		public static String KEY_REMINDER_NOTE = "note";
		//		public static String KEY_REMINDER_LOCATIONNAME = "locationName";
		//		public static String KEY_REMINDER_RANGE = "range";
		//		public static String KEY_REMINDER_ACTIVE = "active";
		//		public static String KEY_REMINDER_ID = "id";

		// Get all the extras passed in.  Could also just set this before the activity is opened
		// (i.e., when the item is selected)

		if (getIntent().getExtras() != null) {
			id = getIntent().getExtras().getLong(Constants.KEY_REMINDER_ID, -1);
			note = getIntent().getExtras().getString(Constants.KEY_REMINDER_NOTE);
			location = getIntent().getExtras().getString(Constants.KEY_REMINDER_LOCATIONNAME);
			active = getIntent().getExtras().getBoolean(Constants.KEY_REMINDER_ACTIVE);
			range = getIntent().getExtras().getInt(Constants.KEY_REMINDER_RANGE);
			vibrate = getIntent().getExtras().getBoolean(Constants.KEY_REMINDER_VIBRATE);
			playRingtone = getIntent().getExtras().getBoolean(Constants.KEY_REMINDER_PLAY_RINGTONE);
			ringtone = getIntent().getExtras().getString(Constants.KEY_REMINDER_RINGTONE);
		}

		// If no ID, then we're creating a new reminder, so nix the Delete button
		if (id == -1) {
			Button deleteButton = (Button) findViewById(R.id.deleteButton);
			deleteButton.setVisibility(View.GONE);
		}

		CheckBoxPreference activePref = (CheckBoxPreference) getPreferenceManager().findPreference("pref_key_active");
		activePref.setChecked(active);
		activePref.setOnPreferenceChangeListener(activeChangeListener);

		EditTextPreference notePref = (EditTextPreference) getPreferenceManager().findPreference("pref_key_reminder_note");
		notePref.setSummary(note);
		notePref.setText(note);
		notePref.setOnPreferenceChangeListener(noteChangeListener);

		EditTextPreference rangePref = (EditTextPreference) getPreferenceManager().findPreference("pref_key_range");
		rangePref.setSummary(Integer.toString(range));
		rangePref.setText(Integer.toString(range));
		rangePref.setOnPreferenceChangeListener(rangeChangeListener);

		CheckBoxPreference vibratePref = (CheckBoxPreference) getPreferenceManager().findPreference("pref_key_vibrate");
		vibratePref.setChecked(vibrate);
		vibratePref.setOnPreferenceChangeListener(vibrateChangeListener);

		CheckBoxPreference playRingtonePref = (CheckBoxPreference) getPreferenceManager().findPreference("pref_key_play_ringtone");
		playRingtonePref.setChecked(playRingtone);
		playRingtonePref.setOnPreferenceChangeListener(playRingtoneChangeListener);

		RingtonePreference ringtonePref = (RingtonePreference) getPreferenceManager().findPreference("pref_key_ringtone");
		ringtonePref.setOnPreferenceChangeListener(ringtoneChangeListener);

		List<Long> locationIds = db.getAllLocationIDs();
		List<String> locationNames = new ArrayList<String>();
		for (Long id : locationIds) {
			LocationInfo locationInfo = db.getLocationById(id);
			locationNames.add(locationInfo.getName());
		}
		locationNames.add(ADD_NEW_LOCATION);

		ListPreference locationPref = (ListPreference) getPreferenceManager().findPreference("pref_key_location");
		locationPref.setSummary(location);
		CharSequence locationArray[] = locationNames.toArray(new CharSequence[locationNames.size()]);
		locationPref.setEntries(locationArray);
		locationPref.setEntryValues(locationArray);
		locationPref.setValue(location);

		locationPref.setOnPreferenceChangeListener(locationChangeListener);

		//		// Set the sharedprefs so that the values will show up in the edit box
		//		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		//		Editor editor = sharedPrefs.edit();
		//		LOG.info("Note from sharedPrefs is: "+sharedPrefs.getString("pref_key_reminder_note", "(not set)"));
		//		editor.putString("pref_key_reminder_note", note);
		//
		//		editor.commit();
		//		LOG.info("After commit, note from sharedPrefs is: "+sharedPrefs.getString("pref_key_reminder_note", "(not set)"));

	}

	Preference.OnPreferenceChangeListener activeChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			LOG.info("Active changed to: "+newValue);
			active = (Boolean)newValue;

			return true;
		}
	};

	Preference.OnPreferenceChangeListener vibrateChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			LOG.info("Vibrate changed to: "+newValue);
			vibrate = (Boolean)newValue;

			return true;
		}
	};

	Preference.OnPreferenceChangeListener playRingtoneChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			LOG.info("PlayRingtone changed to: "+newValue);
			playRingtone = (Boolean)newValue;

			return true;
		}
	};

	Preference.OnPreferenceChangeListener ringtoneChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			LOG.info("Ringtone changed to: "+newValue);
			ringtone = (String)newValue;

			return true;
		}
	};

	Preference.OnPreferenceChangeListener noteChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			LOG.info("Note changed to: "+newValue);
			note = (String)newValue;
			preference.setSummary(note);

			return true;
		}
	};

	Preference.OnPreferenceChangeListener rangeChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			LOG.info("Range changed to: "+newValue);
			range = Integer.parseInt((String)newValue);
			preference.setSummary((String)newValue);

			return true;
		}
	};

	Preference.OnPreferenceChangeListener locationChangeListener = new OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			LOG.info("Location changed to: "+newValue);

			if (((String)newValue).equals(ADD_NEW_LOCATION)){
				LOG.info(ADD_NEW_LOCATION+" clicked");

				Intent intent = new Intent(getApplicationContext(), ChooseLocationMapActivity.class);

				int requestCode = Constants.CODE_MAP_POINT_REQUESTED;
				startActivityForResult(intent, requestCode);
			}
			else {
				location = (String) newValue;
				preference.setSummary((String)newValue);
			}
			return true;
		}
	};

	private void configureButtons() {
		Button saveButton = (Button) findViewById(R.id.saveButton);
		saveButton.setOnClickListener(saveClickListener);

		Button cancelButton = (Button) findViewById(R.id.cancelButton);
		cancelButton.setOnClickListener(cancelClickListener);

		Button deleteButton = (Button) findViewById(R.id.deleteButton);
		deleteButton.setOnClickListener(deleteClickListener);
	}

	private OnClickListener saveClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			LOG.info("Saving Reminder info");
			DbRemindersAccessor database = new DbRemindersAccessor(getApplicationContext());

			// If new reminder, create a new one
			if (id == -1) {
				database.createReminder(note, location, range, playRingtone, ringtone, vibrate, active);
			}

			// If not, update existing one
			else {
				database.updateReminder(id, note, location, range, playRingtone, ringtone, vibrate, active);
			}

			finish();
		}

	};

	private OnClickListener cancelClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			LOG.info("Cancelling");
			finish();
		}

	};

	private OnClickListener deleteClickListener = new OnClickListener() {
		@Override
		public void onClick(View arg0) {
			LOG.info("Deleting Reminder info, id "+id);
			DbRemindersAccessor database = new DbRemindersAccessor(getApplicationContext());

			database.deleteReminder(id);

			finish();
		}		

	};

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		LOG.info("OnActivityResult returned. RequestCode: "+requestCode+", ResultCode: "+resultCode);

		if (requestCode == Constants.CODE_MAP_POINT_REQUESTED) {
			if (resultCode == Constants.CODE_MAP_POINT_RETURNED) {
				if (data != null) {
					newLocationLatE6 = data.getIntExtra(Constants.KEY_MAP_CHOSEN_POINT_LAT, -1);
					newLocationLonE6 = data.getIntExtra(Constants.KEY_MAP_CHOSEN_POINT_LON, -1);
					LOG.info("Point returned from map: "+newLocationLatE6+","+newLocationLonE6);

					// TODO: Reverse GeoCode the lat/lon to an address
					newLocationAddress = "123 Fake St.";

					// Prompt for Name - the handler will take care of creating the new Location Favorite
					// The reason we're using the old-style showDialog here is because PreferenceActivity does not contain
					// SupportFragment support (as in, the Fragment class from the Android Support Libraries). So if I
					// wanted to use DialogFragments, I could only support Android versions back to Honeycomb.
					removeDialog(Constants.DIALOG_NAME_LOCATION);
					showDialog(Constants.DIALOG_NAME_LOCATION);

				}
				else {
					LOG.info("Map activity returned null intent.");
				}

			}
		}
	}


	@Override
	protected Dialog onCreateDialog(int id, Bundle bundle) {
		Dialog dialog = null;

		switch (id) {
		case Constants.DIALOG_NAME_LOCATION:
			AlertDialog.Builder nameDialogBuilder = new AlertDialog.Builder(this);
			LayoutInflater inflater = this.getLayoutInflater();

			nameDialogBuilder.setView(inflater.inflate(R.layout.namepromptdialoglayout, null))
			.setTitle("Give this location a name")
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					EditText nameEditText = (EditText) ((Dialog)dialog).findViewById(R.id.nameEditText);
					newLocationName = nameEditText.getText().toString();
					
					// Call the rest of the stuff
					onFinishEditDialog();
				}
			})
			.setNegativeButton("Cancel", null);
			dialog = nameDialogBuilder.create();
			dialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface arg0) {
					LOG.info("onDismissCalled for nameDialogBuilder dialog");
				}
			});

			break;

		default:
			LOG.info("Unknown dialog ID:" + id);
		}

		return dialog;
	}

	private String newLocationName;
	private String newLocationAddress;
	private Integer newLocationLatE6;
	private Integer newLocationLonE6;


	public void onFinishEditDialog() {
		LOG.info("onFinishEditDialog called for: "+newLocationName);

		if (newLocationName == null) {
			LOG.info("New location name is null. Exiting.");
			return;
		}

		// Store new location in DB
		LocationInfo newLocInfo = db.createLocation(newLocationName, newLocationAddress, newLocationLatE6 / 1E6, newLocationLonE6 / 1E6, true);

		LOG.info("Created new Location: "+newLocInfo.getName());

		ListPreference locationPref = (ListPreference) getPreferenceManager().findPreference("pref_key_location");
		locationPref.setSummary(newLocationName);
		location = newLocationName;

		newLocationName = null;
		newLocationLatE6 = null;
		newLocationLonE6 = null;
		newLocationAddress = null;
	}
}
