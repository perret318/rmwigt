package com.paulperret.locationreminder.db;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.paulperret.locationreminder.common.ConfigureLog4J;
import com.paulperret.locationreminder.common.LocationActionDialogFragment;
import com.paulperret.locationreminder.data.LocationInfo;

public class DbLocationsAccessor {
	private DbHelper dbHelper;
	private SQLiteDatabase database;

	private static final Logger LOG = Logger.getLogger(DbLocationsAccessor.class);
	ConfigureLog4J configlog = new ConfigureLog4J();

	public DbLocationsAccessor(Context context) {
		dbHelper = new DbHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public void closeConnections() {
		database.close();
	}

	private ContentValues createValues(String name, String address, double latitude, double longitude, boolean favorite) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.DB_LOCATIONS_COL_NAME, name);
		values.put(DbHelper.DB_LOCATIONS_COL_ADDRESS, address);
		values.put(DbHelper.DB_LOCATIONS_COL_LATITUDE, latitude);
		values.put(DbHelper.DB_LOCATIONS_COL_LONGITUDE, longitude);
		values.put(DbHelper.DB_LOCATIONS_COL_FAVORITE, favorite);

		return values;
	}
	
	public LocationInfo createLocation(String name, String address, double latitude, double longitude, boolean favorite) {

		ContentValues values = createValues(name, address, latitude, longitude, favorite);
		
		long id = database.insert(DbHelper.DB_TABLE_LOCATIONS, null, values);

		if (id < 0) {
			throw new IllegalStateException("Failed to create DB entry for new location");
		}
		else {
			LocationInfo newLocation = new LocationInfo(name, address, latitude, longitude, favorite, id);
			return newLocation;
		}
	}

	public boolean deleteLocation(long id) {
		int count = database.delete(DbHelper.DB_TABLE_LOCATIONS, DbHelper.DB_LOCATIONS_COL_ID + " = " + id, null);
		return (count > 0);
	}

	public List<Long> getAllLocationIDs() {
		List<Long> idList = new ArrayList<Long>();

		// Get cursor to iterate over list of locations in the DB, then throw all the IDs in a list
		Cursor cursor = database.query(DbHelper.DB_TABLE_LOCATIONS, new String[] { DbHelper.DB_LOCATIONS_COL_ID },
				null, null, null, null, null);
		while (cursor.moveToNext()) {
			long locationId = cursor.getLong(cursor.getColumnIndex(DbHelper.DB_LOCATIONS_COL_ID));
			idList.add(locationId);
		}

		cursor.close();
		return idList;
	}

	public LocationInfo getLocationByName(String name) {
		
		// TODO: Should probably throw an exception here
		if (name == null) {
			LOG.error("getLocationByName name passed is null");
			return null;
		}
		
		List<Long> idList = getAllLocationIDs();
		
		for (Long id : idList) {
			LocationInfo locationInfo = getLocationById(id);
			if (locationInfo.getName().equals(name)) {
				return locationInfo;
			}
		}
		
		return null;
	}
	
	public LocationInfo getLocationById (long id) {
		LocationInfo location = null;
		Cursor cursor = database.query(DbHelper.DB_TABLE_LOCATIONS, getDbColumnNames(),
				DbHelper.DB_LOCATIONS_COL_ID + " = " + id, null, null, null, null);

		if (cursor.getCount() != 1) {
			cursor.close();
		}
		else {
			cursor.moveToFirst();
		    String name = cursor.getString(cursor.getColumnIndex(DbHelper.DB_LOCATIONS_COL_NAME));
		    String address = cursor.getString(cursor.getColumnIndex(DbHelper.DB_LOCATIONS_COL_ADDRESS));
		    double latitude = cursor.getDouble(cursor.getColumnIndex(DbHelper.DB_LOCATIONS_COL_LATITUDE));
		    double longitude = cursor.getDouble(cursor.getColumnIndex(DbHelper.DB_LOCATIONS_COL_LONGITUDE));
		    boolean favorite = cursor.getInt(cursor.getColumnIndex(DbHelper.DB_LOCATIONS_COL_FAVORITE)) == 1;

			location = new LocationInfo(name, address, latitude, longitude, favorite, id);
		}
		return location;
		
	}
	
	public boolean updateLocation(long id, String name, String address, double latitude, double longitude, boolean favorite) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.DB_LOCATIONS_COL_NAME, name);
		values.put(DbHelper.DB_LOCATIONS_COL_ADDRESS, address);
		values.put(DbHelper.DB_LOCATIONS_COL_LATITUDE, latitude);
		values.put(DbHelper.DB_LOCATIONS_COL_LONGITUDE, longitude);
		values.put(DbHelper.DB_LOCATIONS_COL_FAVORITE, favorite);
		
		int updateVal = database.update(DbHelper.DB_TABLE_LOCATIONS, values, 
				DbHelper.DB_LOCATIONS_COL_ID + " = " + id, null);
		
		return (updateVal == 1);
	}


	private static String[] getDbColumnNames() {
		String[] columnArray = new String[] {
				DbHelper.DB_LOCATIONS_COL_NAME,
				DbHelper.DB_LOCATIONS_COL_ADDRESS,
				DbHelper.DB_LOCATIONS_COL_LATITUDE,
				DbHelper.DB_LOCATIONS_COL_LONGITUDE,
				DbHelper.DB_LOCATIONS_COL_FAVORITE};
		
		return columnArray;
	}
}
