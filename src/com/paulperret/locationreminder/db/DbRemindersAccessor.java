package com.paulperret.locationreminder.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.paulperret.locationreminder.data.ReminderInfo;

public class DbRemindersAccessor {
	private DbHelper dbHelper;
	private SQLiteDatabase database;

	public DbRemindersAccessor(Context context) {
		dbHelper = new DbHelper(context);
		database = dbHelper.getWritableDatabase();
	}

	public void closeConnections() {
		database.close();
	}

	private ContentValues createValues(String note, String locationName, int range, boolean playRingtone, String ringtone, boolean vibrate, boolean active) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.DB_REMINDERS_COL_NOTE, note);
		values.put(DbHelper.DB_REMINDERS_COL_LOCATION, locationName);
		values.put(DbHelper.DB_REMINDERS_COL_RANGE, range);
		values.put(DbHelper.DB_REMINDERS_COL_RINGTONE, ringtone);
		values.put(DbHelper.DB_REMINDERS_COL_VIBRATE, vibrate);
		values.put(DbHelper.DB_REMINDERS_COL_PLAY_RINGTONE, playRingtone);
		values.put(DbHelper.DB_REMINDERS_COL_ACTIVE, active);

		return values;
	}
	
	public ReminderInfo createReminder(String note, String locationName, int range, boolean playRingtone, String ringtone, boolean vibrate, boolean active) {

		ContentValues values = createValues(note, locationName, range, playRingtone, ringtone, vibrate, active);
		
		long id = database.insert(DbHelper.DB_TABLE_REMINDERS, null, values);

		if (id < 0) {
			throw new IllegalStateException("Failed to create DB entry for new reminder");
		}
		else {
			ReminderInfo newReminder = new ReminderInfo(note, locationName, range, playRingtone, ringtone, vibrate, active, id);
			return newReminder;
		}
	}

	public boolean deleteReminder(long id) {
		int count = database.delete(DbHelper.DB_TABLE_REMINDERS, DbHelper.DB_REMINDERS_COL_ID + " = " + id, null);
		return (count > 0);
	}

	public boolean setReminderActive(long id, boolean activeFlag) {
		ContentValues values = new ContentValues(1);
		values.put(DbHelper.DB_REMINDERS_COL_ACTIVE, activeFlag);
		int count = database.update(DbHelper.DB_TABLE_REMINDERS, values, DbHelper.DB_REMINDERS_COL_ID + " = " + id, null);
		return (count != 0);
	}


	public List<Long> getAllReminderIDs() {
		List<Long> idList = new ArrayList<Long>();

		// Get cursor to iterate over list of reminders in the DB, then throw all the IDs in a list
		Cursor cursor = database.query(DbHelper.DB_TABLE_REMINDERS, new String[] { DbHelper.DB_REMINDERS_COL_ID },
				null, null, null, null, null);
		while (cursor.moveToNext()) {
			long reminderId = cursor.getLong(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_ID));
			idList.add(reminderId);
		}

		cursor.close();
		return idList;
	}

	public List<Long> getAllActiveReminderIDs() {
		List<Long> idList = new ArrayList<Long>();

		// Get cursor to iterate over list of reminders in the DB, match all that have active set to true (1)
		Cursor cursor = database.query(DbHelper.DB_TABLE_REMINDERS, new String[] { DbHelper.DB_REMINDERS_COL_ACTIVE+" = 1" },
				null, null, null, null, null);
		while (cursor.moveToNext()) {
			long reminderId = cursor.getLong(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_ID));
			idList.add(reminderId);
		}

		cursor.close();
		return idList;
	}

	public ReminderInfo getReminderById (long id) {
		ReminderInfo reminder = null;
		Cursor cursor = database.query(DbHelper.DB_TABLE_REMINDERS, getDbColumnNames(),
				DbHelper.DB_REMINDERS_COL_ID + " = " + id, null, null, null, null);

		if (cursor.getCount() != 1) {
			cursor.close();
		}
		else {
			cursor.moveToFirst();
		    String note = cursor.getString(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_NOTE));
		    String locationName = cursor.getString(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_LOCATION));
		    int range = cursor.getInt(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_RANGE));
		    boolean playRingtone = cursor.getInt(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_PLAY_RINGTONE)) == 1;
		    String ringtone = cursor.getString(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_RINGTONE));
		    boolean vibrate= cursor.getInt(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_VIBRATE)) == 1;
		    boolean active= cursor.getInt(cursor.getColumnIndex(DbHelper.DB_REMINDERS_COL_ACTIVE)) == 1;

			reminder = new ReminderInfo(note, locationName, range, playRingtone, ringtone, vibrate, active, id);
		}
		return reminder;
		
	}
	
	public boolean updateReminder(long id, String note, String locationName, int range, boolean playRingtone, String ringtone, boolean vibrate, boolean active) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.DB_REMINDERS_COL_NOTE, note);
		values.put(DbHelper.DB_REMINDERS_COL_LOCATION, locationName);
		values.put(DbHelper.DB_REMINDERS_COL_RANGE, range);
		values.put(DbHelper.DB_REMINDERS_COL_PLAY_RINGTONE, playRingtone);
		values.put(DbHelper.DB_REMINDERS_COL_RINGTONE, ringtone);
		values.put(DbHelper.DB_REMINDERS_COL_VIBRATE, vibrate);
		values.put(DbHelper.DB_REMINDERS_COL_ACTIVE, active);
		
		int updateVal = database.update(DbHelper.DB_TABLE_REMINDERS, values, 
				DbHelper.DB_REMINDERS_COL_ID + " = " + id, null);
		
		return (updateVal == 1);
	}


	private static String[] getDbColumnNames() {
		String[] columnArray = new String[] {
				DbHelper.DB_REMINDERS_COL_NOTE,
				DbHelper.DB_REMINDERS_COL_LOCATION,
				DbHelper.DB_REMINDERS_COL_RANGE,
				DbHelper.DB_REMINDERS_COL_PLAY_RINGTONE,
				DbHelper.DB_REMINDERS_COL_RINGTONE,
				DbHelper.DB_REMINDERS_COL_VIBRATE,
				DbHelper.DB_REMINDERS_COL_ACTIVE};
		
		return columnArray;
	}

}
