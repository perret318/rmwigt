package com.paulperret.locationreminder.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.paulperret.locationreminder.common.Constants;

public class DbHelper extends SQLiteOpenHelper {

	/*
	 * REMINDERS table
	 */
	public static final String DB_TABLE_REMINDERS = "reminders";

	public static final String DB_REMINDERS_COL_ID = "_id";
	public static final String DB_REMINDERS_COL_NOTE = "note";
	public static final String DB_REMINDERS_COL_LOCATION = "name";
	public static final String DB_REMINDERS_COL_RANGE = "range";
	public static final String DB_REMINDERS_COL_RINGTONE = "ringtone";
	public static final String DB_REMINDERS_COL_VIBRATE = "vibrate";
	public static final String DB_REMINDERS_COL_ACTIVE = "active";
	public static final String DB_REMINDERS_COL_PLAY_RINGTONE = "playRingtone";

	public static final String DB_REMINDERS_CREATE_STRING =
			"CREATE TABLE "+DB_TABLE_REMINDERS+" ("
					+ DB_REMINDERS_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DB_REMINDERS_COL_NOTE + " TEXT, "
					+ DB_REMINDERS_COL_LOCATION + " TEXT, "
					+ DB_REMINDERS_COL_RANGE + " UNSIGNED INTEGER (0, 1000), "
					+ DB_REMINDERS_COL_RINGTONE + " UNSIGNED INTEGER (0, 1), "
					+ DB_REMINDERS_COL_VIBRATE + " UNSIGNED INTEGER (0, 1), "
					+ DB_REMINDERS_COL_PLAY_RINGTONE + " UNSIGNED INTEGER (0, 1), "
					+ DB_REMINDERS_COL_ACTIVE + " UNSIGNED INTEGER (0, 1))";

	/*
	 * LOCATIONS table
	 */
	public static final String DB_TABLE_LOCATIONS = "locations";
	
	public static final String DB_LOCATIONS_COL_ID = "_id";
	public static final String DB_LOCATIONS_COL_NAME = "name";
	public static final String DB_LOCATIONS_COL_ADDRESS = "address";
	public static final String DB_LOCATIONS_COL_LATITUDE = "latitude";
	public static final String DB_LOCATIONS_COL_LONGITUDE = "longitude";
	public static final String DB_LOCATIONS_COL_FAVORITE = "favorite";
	
	public static final String DB_LOCATIONS_CREATE_STRING =
			"CREATE TABLE "+DB_TABLE_LOCATIONS+" ("
			+ DB_LOCATIONS_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ DB_LOCATIONS_COL_NAME + " TEXT, "
			+ DB_LOCATIONS_COL_ADDRESS + " TEXT, "
			+ DB_LOCATIONS_COL_LATITUDE + " REAL, "
			+ DB_LOCATIONS_COL_LONGITUDE + " REAL, "
			+ DB_LOCATIONS_COL_FAVORITE + " UNSIGNED INTEGER (0, 1))";

	
	public DbHelper(Context context) {
		super(context, Constants.DB_NAME_LOCATION_REMINDER, null, Constants.DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DB_REMINDERS_CREATE_STRING);
		db.execSQL(DB_LOCATIONS_CREATE_STRING);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
}
